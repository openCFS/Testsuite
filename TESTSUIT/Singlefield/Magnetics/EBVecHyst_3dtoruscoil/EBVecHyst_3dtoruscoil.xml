<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.cfs++.org/simulation 
    file:/home/kroppert/Devel/CFS_SRC/latest_trunkGit/CFS/share/xml/CFS-Simulation/CFS.xsd">
    

    <documentation>
        <title>Energy based hysteresis 3d torus coil </title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2022-08-15</date>
        <keywords>
            <keyword>magneticScalar</keyword>
            <keyword>hysteresis</keyword>
        </keywords>
        <references></references>
        <isVerified>no</isVerified>
        <description> 3d example of a torus coil, where the source magnetic field Hs
                      is computed via MagEdgePDE (A-based) formulation and the source
                      field is imported in the second sequence step (magnetic scalar
                      potential PDE in Psi formulation). In this second sequence step
                      the hysteretic material model is used.
                      No linesearch is used for this example
         </description>
      </documentation>

    <!-- define which files are needed for simulation input & output-->
    <fileFormats>
        <input>
            <hdf5 fileName="EBVecHyst_3dtoruscoil.h5ref" />
        </input>
        <output>
            <hdf5 id="hdf5"/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>

    <domain geometryType="3d">
        <variableList>
            <var name="i" value="1"/>
            <var name="N" value="100"/>
            <var name="f_coil" value="10"/>
            <var name="A" value="0.002"/>
        </variableList>
        <regionList>
            <region name="V_core" material="iron" />
            <region name="V_coil" material="air"/>
            <region name="V_air" material="air"/>
        </regionList>
        <surfRegionList>
            <surfRegion name="S_z"/>
            <surfRegion name="S_y"/>
            <surfRegion name="S_x"/>
        </surfRegionList>
        <nodeList>
            <nodes name="N_gauge"/>
        </nodeList>
        <coordSysList>
            <cylindric id="cyl1">
                <origin x="0.0" y="0.0" z="0.0"/>
                <zAxis x="0" y="0" z="1"/>
                <rAxis x="1" y="0" z="0"/>
            </cylindric>
        </coordSysList>
    </domain>

    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder> 0 </isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>

    <sequenceStep index="1">
        <analysis>
            <transient>
                <numSteps>3</numSteps>
                <deltaT>0.2</deltaT>
                <allowPostProc>yes</allowPostProc>
            </transient>
        </analysis>

        <pdeList>
            <magneticEdge onlyVacuum="true" >
                <regionList>
                    <region name="V_core" polyId="Hcurl" />
                    <region name="V_coil" polyId="Hcurl"/>
                    <region name="V_air" polyId="Hcurl"/>
                </regionList>

                <bcsAndLoads>
                    <fluxParallel name="S_z"/>
                    <fluxParallel name="S_y"/>
                    <fluxParallel name="S_x"/>
                    <fluxParallel name="S_outer"/>
                </bcsAndLoads>

                <coilList>
                    <coil id="coil1">
                        <source type="current" value="i*N*t*0.3"/>
                        <part id="1">
                            <regionList>
                                <region name="V_coil"/>
                            </regionList>
                            <direction>
                                <analytic coordSysId="cyl1">
                                    <comp dof="phi" value="1.0"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="A"/>
                        </part>
                    </coil>
                </coilList>
                <storeResults>
                     <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magFieldIntensity">
                        <allRegions/>
                    </elemResult> 
                </storeResults>
            </magneticEdge>
        </pdeList>
    </sequenceStep>




    <sequenceStep index="2">
        <analysis>
            <transient initialTime="zero">
                <numSteps>3</numSteps>
                <deltaT>0.2</deltaT>
            </transient>
        </analysis>

        <pdeList>
            <magnetic formulation="Psi">
                <regionList>
                    <region name="V_core" polyId="H1" nonLinIds="nl1"/>
                    <region name="V_coil" polyId="H1"/>
                    <region name="V_air" polyId="H1"/>
                </regionList>

                <nonLinList>
                    <permeability id="nl1" model="EBHysteresisModel" />
                </nonLinList>
            

                <bcsAndLoads>
                    <potential name="N_gauge" value="0"/>
                    <fieldIntensity name="V_air">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant />
                                <!-- <constant /> -->
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>

                    <fieldIntensity  name="V_coil">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant />
                                <!-- <constant /> -->
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>

                    <fieldIntensity name="V_core">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant />
                                <!-- <constant /> -->
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>
                </bcsAndLoads>

                <storeResults>
                    <nodeResult type="magPotential">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magFieldIntensity">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </magnetic>
        </pdeList>

        <linearSystems>
            <system>
              <solutionStrategy>
                  <standard>
                      <nonLinear logging="yes" method="fixPoint" >
                                  <lineSearch type="none"/>
                                  <incStopCrit> 1e-3</incStopCrit>
                                  <resStopCrit> 1e-3</resStopCrit>
                                  <maxNumIters>100</maxNumIters>
                                  <abortOnMaxNumIters>yes</abortOnMaxNumIters>
                      </nonLinear>
                  </standard>
              </solutionStrategy>
            </system>
          </linearSystems>
    </sequenceStep>


</cfsSimulation>
