<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Channel flow - Linearized Navier-Stokes with basic flow</title>
    <authors>
      <author>Manfred Kaltenbacher</author>
    </authors>
    <date>2013-12-31</date>
    <keywords>
      <keyword>CFD</keyword>
    </keywords>    
    <references>
    </references>
    <isVerified>yes</isVerified>
    <description> 
       Simple channel flow: basic flow is given and the perturbed flow
       is computed  
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <!--<gmsh fileName="Channel2dMeanFlowPerturbed.msh"/>-->
      <hdf5 fileName="Channel2dMeanFlowPerturbed.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane" printGridInfo="yes">
    <variableList>
      <var name="H"    value="1"/>
      <var name="L"    value="2"/>
    </variableList>
    <regionList>
      <region name="channel" material="FluidMat"/>
    </regionList>
    
    <nodeList>
      <nodes name="pres_in">
        <!--list>
          <freeCoord comp="y" start="0" stop="H" inc="0.0625"/>
          <fixedCoord comp="x" value="0"/>
          </list-->
        <coord x="0.0" y="0.0"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="velPolyId">
       <gridOrder/>
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="presPolyId">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="velIntegId">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="presIntegId">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <fluidMech systemId="fluid" formulation="perturbed" enableC2="false" factorC1="1.0">
        <regionList>
          <region name="channel" flowId="myFlow"/>
        </regionList>
        
        <presSurfaceList>
          <presSurf name="bottom"/>
          <presSurf name="top"/>
          <presSurf name="left"/>
          <presSurf name="right"/>
        </presSurfaceList>

        <flowList>
          <flow name="myFlow">
            <comp dof="x" value="10*sin(pi*y)*sin(pi*y)*sin(pi*y)"/>
          </flow>
        </flowList>
        
        <bcsAndLoads>
          <!-- New Section -->
          <noPressure name="pres_in"/>
          <noSlip name="top">
            <comp dof="x"/>
          </noSlip>
          <noSlip name="bottom">
            <comp dof="x"/>
          </noSlip>
          <noSlip name="left">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
          
          <velocity name="bottom">
            <comp dof="y" value="1e-6*(-(x-1)*(x-1)+1)"/>
          </velocity>
          <velocity name="top">
            <comp dof="y" value="1e-6*(-(x-1)*(x-1)+1)"/>
          </velocity>
          
          <!-- Old Section -->
          <!--<dirichletHom name="pres_in" quantity="fluidMechPressure"/>
          
          <dirichletInhom name="bottom" quantity="fluidMechVelocity" dof="y" value="1e-6*(-(x-1)*(x-1)+1)"/>
          <dirichletInhom name="top" quantity="fluidMechVelocity" dof="y" value="1e-6*(-(x-1)*(x-1)+1)"/>
          <dirichletHom name="top" quantity="fluidMechVelocity" dof="x"/>
          <dirichletHom name="bottom" quantity="fluidMechVelocity" dof="x"/>
          <dirichletHom name="inlet" quantity="fluidMechVelocity" dof="x"/>
          <dirichletHom name="inlet" quantity="fluidMechVelocity" dof="y"/>-->
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions/>         
          </nodeResult>
          <nodeResult type="meanFluidMechVelocity">
            <allRegions/>         
          </nodeResult>
          <elemResult type="fluidMechStress">
            <allRegions/>
          </elemResult>
          <elemResult type="fluidMechStrainRate">
            <allRegions/>
          </elemResult>
          <!--sensorArray fileName="vel-line.txt" type="fluidMechVelocity">
            <parametric>
            <list comp="x" start="1" stop="1" inc="0"/>
            <list comp="y" start="0" stop="1" inc="0.01"/>
            </parametric>
          </sensorArray-->          
        </storeResults>
      </fluidMech>
    </pdeList>
    <linearSystems>
      <system id="fluid">
        <solutionStrategy>
          <standard>
            <setup idbcHandling="elimination" staticCondensation="no"/>
            <!--exportLinSys baseName="stokes_mat_pres"/-->
            <matrix storage="sparseNonSym"/>
            <solver id="default"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <umfpack/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
