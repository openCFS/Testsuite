<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title> feature mapping using linear spline ("spaghetti") with p-norm for feature aggregation
    </title>
    <authors>
      <author>Jannis Greifenstein</author>
    </authors>
    <date>2022-06-01</date>
    <keywords>
      <keyword>optimization</keyword>
    </keywords>
    <references>
    </references>
    <isVerified>no</isVerified>
        <description> Tests bending constraint for spaghetti which limits the curvature </description>
  </documentation>

  <fileFormats>
    <output>
	<hdf5/>    
	<info/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region material="99lines" name="mech"/>
    </regionList>
    <nodeList>
      <nodes name="load">
        <coord x="2.0" y="0.5"/>
      </nodes>
      <nodes name="fix1">
        <coord x="0.0" y="1.0"/>
      </nodes>
      <nodes name="fix2">
        <coord x="0.0" y="0.0"/>
      </nodes>
    </nodeList>
  </domain>


  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="planeStress">
        <regionList>
          <region name="mech"/>
        </regionList>

        <bcsAndLoads>
           <fix name="west"> 
              <comp dof="x"/> <comp dof="y"/> 
           </fix>
           <force name="load" >
             <comp dof="y" value="-1"/>
           </force>
           
        </bcsAndLoads>
        <storeResults>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_1">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_3">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_4">
            <allRegions/>
          </elemResult>
          <elemResult type="mechTensor">
            <allRegions/>
          </elemResult>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
<!--     <linearSystems> -->
<!--       <system> -->
<!--         <solverList> -->
<!--           <cholmod/> -->
<!--         </solverList> -->
<!--       </system> -->
<!--     </linearSystems> -->
  </sequenceStep>
    
  <optimization>
    <costFunction type="compliance" task="minimize" sequence="1" linear="false">
      <stopping queue="55" value="0.0001" type="relativeCostChange" />
    </costFunction>

    <constraint type="bending"  bound="upperBound" value="0.205" design="spaghetti" /> 
<!--     <constraint type="bending" design="spaghetti" bound="upperBound" value="2" linear="false" mode="observation"/> -->
    <constraint type="volume" design="density" bound="upperBound" value="0.45" mode="constraint" linear="false"/> 
   
    <optimizer type="snopt" maxIterations="1">
      <!-- nCon is only for compatabiliy of old test cases -->
      <snopt iteration="nCon">
        <option name="major_optimality_tolerance" type="real" value="1e-8"/>
        <option type="integer" name="verify_level" value="-1"/>
      </snopt>
    </optimizer>
    
    <ersatzMaterial region="mech" material="mechanic" method="spaghetti">
      <spaghetti combine="p-norm" boundary="poly" transition=".14" radius=".25"  gradplot="true">
        <python file="spaghetti.py" path="cfs:share:python" >
          <!-- silent disables command line output from pythons-->
          <option key="silent" value="1"/>
          <option key="order" value="2"/>
          <option key="p" value="8"/>
          <option key="gradient_check" value="0"/>
        </python> 
        <noodle segments="2">
          <node dof="x" initial="0.03" upper="2" lower="0" tip="start"/>
          <node dof="y" initial="0.9" upper="1" lower="0" tip="start"/>
          <node dof="x" initial="1.9" upper="2" lower="0" tip="end"/>
          <node dof="y" initial="0.5" upper="1" lower="0" tip="end"/>
          <profile initial=".15"  lower=".1" upper=".2" />
          <normal initial=".2" lower="-.2" upper=".2" /> 
        </noodle>
        <noodle segments="1">
          <node dof="x" initial="0.08" upper="2" lower="0" tip="start"/>
          <node dof="y" initial="0.2" upper="1" lower="0"  tip="start"/>
          <node dof="x" initial="1." upper="2" lower="0"  tip="end"/>
          <node dof="y" initial="0.8" upper="1" lower="0" tip="end"/>
          <profile fixed=".15"/>
        </noodle>
        <noodle segments="1">
          <node dof="x" initial="0.08" upper="2" lower="0" tip="start"/>
          <node dof="y" initial="0.8" upper="1" lower="0"  tip="start"/>
          <node dof="x" initial="1." upper="2" lower="0"  tip="end"/>
          <node dof="y" initial="0.2" upper="1" lower="0" tip="end"/>
          <profile fixed=".15"/>
        </noodle>
        <noodle segments="2">
          <node dof="x" initial="0.03" upper="2" lower="0" tip="start"/>
          <node dof="y" initial="0.1" upper="1" lower="0"  tip="start"/>
          <node dof="x" initial="1.95" upper="2" lower="0"  tip="end"/>
          <node dof="y" initial="0.5" upper="1" lower="0" tip="end"/>
          <profile initial=".15"  lower=".1" upper=".2" />
          <normal initial="-0.2" lower="-0.2" upper="0.2"/>
        </noodle>

      </spaghetti>
    
      <design name="density" initial=".5" physical_lower="1e-4" upper="1.0"/>

      <transferFunction type="identity" application="mech" design="density" />
      
       <result value="design" id="optResult_1" design="density" /> 
       <result value="costGradient" id="optResult_1" design="density" /> 
      <export save="all" write="iteration"/>
      
    </ersatzMaterial>
    <commit mode="forward" stride="100"/>
  </optimization>
</cfsSimulation>
