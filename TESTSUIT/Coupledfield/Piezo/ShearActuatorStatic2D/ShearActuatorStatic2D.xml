<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
  <documentation>
    <title>Piezoelectric Shear Actuator - 2D</title>
    <authors>
      <author>ahauck</author>
    </authors>
    <date>2012-03-10</date>
    <keywords>
      <keyword>piezo</keyword>
    </keywords>
    <references>
      @PHDTHESIS{piefort01:1,
      author = {Piefort, V},
      title = {{Finite Element Modelling of Piezoelectric Active Structures}},
      school = {Department of Mechanical Engineering and Robotics, Universite Libre
      de Bruxelles},
      year = {2001},
      }
      
      @ARTICLE{benjeddou97:1,
      author = {Benjeddou, A., Trindade, M.A., Ohayon, R.},
      title = {A unified beam finite element model for extension and shear piezoelectric
      actuation mechanisms},
      journal = {Journal of Intelligent Material Systems and Structures},
      year = {1997},
      volume = {8},
      pages = {1012-1025},
      number = {12},
      }
    </references>
    <isVerified>yes</isVerified>
    <description>
      This is a model of a piezoelectric shear actuator, which consists
      of a PZT-5H layer (polarized in length direction!), wrapped on top
      and bottom by a layer of aluminum.
      
      The goal is to compare the tip deflection, with a reference value
      (analytical calculation) of 1.19e-7m. 
    </description>
  </documentation>
  
  <fileFormats>
    <output>
      <hdf5 id ="h"/>
      <text id="txt"/>      
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="core" material="PZT-5H">
        <!--
          Perform correct material rotation:
          1) the polarization(3-direction) has to be in x-direction 
          2) the original 1-direction has to be mapped to the y-direction
          -->
        <matRotation alpha="0" beta="90" gamma="90"/>
      </region>
      <region name="skin" material="Aluminum"/>
    </regionList>
  </domain>
  
  <fePolynomialList>
    <Legendre>
      <isoOrder>2</isoOrder>
    </Legendre>
  </fePolynomialList>
  <sequenceStep>
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <!-- Important: In the publication, a plane-stress 
           approximation is assumed (see annotation 
           on p. 1015, after eq (13) ). -->
      <mechanic subType="planeStress">
        <regionList>
          <region name="core"/>
          <region name="skin"/>
        </regionList>
        
        <bcsAndLoads>
          <fix name="left">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
            <nodeList>
              <nodes name="hist" outputIds="txt"/>
            </nodeList>
            
          </nodeResult>
        </storeResults>
        
      </mechanic>
      
      <electrostatic>
        <regionList>
          <region name="core"/>
        </regionList>
        
        <bcsAndLoads>
          <potential name="piezo-bot" value="20"/>
          <ground name="piezo-top"/>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="elecPotential">
            <allRegions/>
          </nodeResult>
          <elemResult type="elecFieldIntensity">
            <allRegions/>
          </elemResult>
        </storeResults>
      </electrostatic>
      
    </pdeList>
    <couplingList>
      
      <direct>
        <piezoDirect>
          <regionList>
            <region name="core"/>
          </regionList>
        </piezoDirect>
      </direct>
    </couplingList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>

  </sequenceStep>
  
</cfsSimulation>
