<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
  
 <documentation>
   <title>Submerged Steel Ring</title>
   <authors>
     <author>ahauck</author>
   </authors>
   <date>2010-06-10</date>
   <keywords>
     <keyword>mechanic-acoustic</keyword>
   </keywords>
   <references>Ansys Verification Example VM 177 </references>
   <isVerified>no</isVerified>
   <description>
     A steel ring is submerged in a compressible fluid (water). 
     The lowest natural frequency for x-y plane bending modes of the 
     fluid-structure system is determined. Due to symmetry, just one 
     quarter is modeled.

     The theoretical solution is  35.62 Hz. It is observable in all the 
     three observation points at 0°, 45° and 90° (see history files).
     
     In our simulation we obtain the first eigenfrequency at 
     36.1 Hz (= 1.3 % error).
     
     The detailed description can be found in ansys-vm-177-description.pdf.
   </description>
 </documentation>
   
  <fileFormats>
    <input>
      <hdf5 fileName="RingHarm2d.h5"/>
    </input>
    <output>
      <hdf5/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">

    <regionList>
      <region name="ring" material="steel"/>
      <region name="water" material="water"/>
    </regionList>

    <surfRegionList>
      <surfRegion name="coupleEl"/>
      <surfRegion name="outer"/>
    </surfRegionList>

    <nodeList>
      <nodes name="observer1"/>
      <nodes name="load1"/>
      <nodes name="observer2"/>
      <nodes name="load2"/>
      <nodes name="observer3"/>
      <nodes name="fix1"/>
      <nodes name="fix2"/>
    </nodeList>
  </domain>

<fePolynomialList>
  <Lagrange id="default">
    <isoOrder serendipity="false">2</isoOrder>
  </Lagrange>
  
  <Legendre id="mech">
    <isoOrder>3</isoOrder>
  </Legendre>
 
</fePolynomialList>
  <sequenceStep>

    <analysis>
     <harmonic>
        <numFreq>   30 </numFreq>
        <startFreq> 35.5 </startFreq>
        <stopFreq>  36.5 </stopFreq>
        <sampling> linear </sampling>
      </harmonic> 
    </analysis>

    <pdeList>

      <mechanic subType="planeStrain">
        <regionList>
          <region name="ring" polyId="mech"/>
        </regionList>
        <!--regionList>
          <region name="ring" softeningId="1"/>
        </regionList>
        <softeningList>
          <icModesTW id="1"/>
        </softeningList-->
        <bcsAndLoads>
          <fix name="fix1">
            <comp dof="y"/>
          </fix>
          <fix name="fix2">
            <comp dof="x"/>
          </fix>
          <force name="load1">
            <comp dof="x" value="1"/>
          </force>
          <force name="load1">
            <comp dof="y" value="-1"/>
          </force>          
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement" complexFormat="realImag">
            <allRegions/>
            <nodeList>
              <nodes name="observer1"/>
              <nodes name="observer2"/>
              <nodes name="observer3"/>
            </nodeList>
          </nodeResult>
          <nodeResult type="mechVelocity" complexFormat="realImag">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
      
      
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="water"/>
        </regionList>
        <bcsAndLoads>
          <soundSoft name="outer"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure" complexFormat="realImag">
            <allRegions/>
          </nodeResult>
          <elemResult type="acouVelocity" complexFormat="realImag">
            <allRegions/>
          </elemResult>
        </storeResults>
      </acoustic>

    </pdeList>

    <couplingList>
      <direct>
        <acouMechDirect>
          <surfRegionList>
            <surfRegion name="coupleEl"/>
          </surfRegionList>
        </acouMechDirect>
      </direct>
    </couplingList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>

  </sequenceStep>
</cfsSimulation>
