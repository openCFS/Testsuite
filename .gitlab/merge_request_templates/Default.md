## Description

( Summarise the changes introduced in this merge request )

__CFS merge request__: ( paste a link to the merge request in the upstream CFS project )

## Check before assigning to a maintainer for review

* [ ] The history is clean
* [ ] New test cases are well-documented and understandable

## Maintainer checks before merge

* [ ] The _Testsuite_ pipeline passed and is recent (re-run the `cfs-pipeline` trigger job if necessary)
    If it failed to trigger an upstream pipeline, possible errors are
    -  branch names do not match
    -  there is no open merge request in the _CFS_ project
* [ ] Check the pipeline of the upstream _CFS_ merge request
